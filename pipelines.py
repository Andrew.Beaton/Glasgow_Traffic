# -*- coding: utf-8 -*-
"""
Created on Sun Feb 13 12:53:41 2022

@author: andrewb
"""
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import OneHotEncoder
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import StandardScaler

import functions as func


    
def Speed_pipeline():
    #Pipeline Speed limit
    return  Pipeline(steps = [
        ("imputer_speed",SimpleImputer(strategy = "most_frequent")),
        ("scaler_speed", StandardScaler())
        ])

def date_time_pipeline():
    #Pipeline Speed limit
    return  Pipeline(steps = [
        ("imputer_date_time",SimpleImputer(strategy = "most_frequent")),
        ("scaler_date_time", StandardScaler())
        ])



def pipeline_main(final_cols):
    
    #Pipeline for categorical Data 
    cat_transformer = Pipeline(steps = [
        ("imputer",SimpleImputer(strategy = "most_frequent")),
        ("onehot" , OneHotEncoder())
        ])
    
    
    
    #Pipeline for Numerical Data  
    num_transformer =  Pipeline(steps = [
         ("imputer",SimpleImputer(strategy = "most_frequent")),
         ("scaler", StandardScaler())
         ])
    
    

    #Creating the pipeline for the speed variable
    speed_pipeline = Speed_pipeline()

    #Combine Pre processing pipelines 
    preprocessor = ColumnTransformer(
        transformers = [
            ("num", num_transformer,final_cols["numerical_cols_final"]),
            ("cat",cat_transformer,final_cols["cat_cols_final"]),
            ("speed",speed_pipeline,final_cols["Speed_col"])
            ])
    
    
    
    return preprocessor
