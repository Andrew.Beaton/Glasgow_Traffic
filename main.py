# -*- coding: utf-8 -*-
"""
Created on Mon Feb  7 19:20:49 2022

@author: andrewb
"""

import pandas as pd 
 



import pre_process as pre_pro
import pipelines  as pipe
import models as mod
import EDA as eda
# Importing custom fucntions 




#Path to traffice data source 
datapath = "Z:\\Andrew\\python\\Glasgow_Traffic\\Road_Safety_Accidents.csv"
#datapath = "/home/andrew/Desktop/Road_Safety_Accidents.csv"
#Loads data into pandas
raw_traffic = pd.read_csv(datapath)

def main ():
    
    traffic_data = pre_pro.cleaning_initial(raw_traffic)
    post_pre_process = pre_pro.ml_prep(traffic_data)
    inspect =post_pre_process
    pipeline  = pipe.pipeline_main(post_pre_process["final_cols"])
    #return inspect
    mod.model_main(post_pre_process["split_data"], pipeline)
    

if __name__ == "__main__" :
        inspect = main()
