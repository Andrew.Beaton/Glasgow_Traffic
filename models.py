# -*- coding: utf-8 -*-
"""
Created on Sun Feb 13 13:55:09 2022

@author: andrewb
"""


from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report
from sklearn.svm import LinearSVC
from sklearn.pipeline import Pipeline

from sklearn.model_selection import GridSearchCV
import time 
import model_analysis as mod_an

def modelpipeline(model,pipe):
    var_pipe = Pipeline(
        steps = [("preprocessor",pipe),
                 ("TestedModel",model)
                 ])
    return var_pipe

def model_main(split_data,pipe):
    """ Executes the models and prints accuracy scores  """
    logistic_regression =  LogisticRegression (random_state = 0,max_iter=1000)

    linear_SVC = LinearSVC(random_state = 0,max_iter=1000)

    #~~~~~~~~~~
    #logistic Regression
    #~~~~~~~~~~
    t0=time.time()
    regpipe = modelpipeline(logistic_regression,pipe)
    regpipe.fit(split_data["train_features"],split_data["train_labels"])
    predictions = regpipe.predict(split_data["test_features"])
    
    """ test labels need to be one hot encoded before confusion matrix
    Need to work this into the pipeline process """
    #plot_confusion_matrix(logistic_regression,
    #                     split_data["test_labels"],predictions )
    
    
    mod_an.general_overview_classification(logistic_regression, 
                            split_data["test_labels"], 
                            predictions)

    mod_an.confusion_matrix_plot(logistic_regression, 
                            split_data["test_labels"], 
                            predictions)
    
    print('Time taken :' , time.time()-t0)


    #~~~~~~~~~~
    #SVC
    #~~~~~~~~~~
    t1 = time.time()
    svcpipe = modelpipeline(linear_SVC,pipe)
    svcpipe.fit(split_data["train_features"],split_data["train_labels"])

    preductions_SVC = svcpipe.predict(split_data["test_features"])

    mod_an.general_overview_classification(linear_SVC, 
                            split_data["test_labels"], 
                            preductions_SVC)
    
    mod_an.confusion_matrix_plot(linear_SVC, 
                            split_data["test_labels"], 
                            preductions_SVC)

    print('Time taken :' , time.time()-t0)
    
    
    #~~~~~~~~~~
    #Random Forest
    #~~~~~~~~~~
    
    par_grid = {
        'n_estimators' : [10,200,500,1000],
        'max_features': ['auto', 'sqrt', 'log2'],
        }
        
    grid_rfc = GridSearchCV(estimator = RandomForestClassifier(random_state=42), 
                            param_grid = par_grid,
                            error_score="raise",
                            refit = True, 
                            verbose = 3)
    
    grid_rfc_pipe = modelpipeline(grid_rfc,pipe)
    grid_rfc_pipe.fit(split_data["train_features"],split_data["train_labels"])
    grid_predict = grid_rfc_pipe.predict(split_data["test_features"])
    classification_report (split_data["test_labels"],grid_predict)
    
    """
    
    RFC = RandomForestClassifier(n_estimators = 20 , n_jobs = 4 )
    #execute
    rfc_pipe = modelpipeline(RFC, pipe)
    
    rfc_pipe.fit(split_data["train_features"],split_data["train_labels"])
    
    predictions_rfc = rfc_pipe.predict(split_data["test_features"])

    mod_an.general_overview_classification(RFC, 
                            split_data["test_labels"], 
                            predictions_rfc)
    
    mod_an.confusion_matrix_plot(RFC, 
                            split_data["test_labels"], 
                            predictions_rfc)
    """
    
    