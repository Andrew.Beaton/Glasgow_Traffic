# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 12:20:33 2022

@author: andrewb
"""
 
import pandas as pd
 

def var_check(data):
    data  = data.loc[:, data.apply(pd.Series.nunique) != 1 ]
    return data

def datatime_transform(traffic_data):
    traffic_datetime = pd.DataFrame()
    traffic_datetime["Accident_year"] = traffic_data["DATE_"].dt.year.astype("int")
    traffic_datetime["Accident_Month"] = traffic_data["DATE_"].dt.month.astype("int")
    traffic_datetime["Accident_Day"] = traffic_data["DATE_"].dt.day.astype("int")
    traffic_datetime["Accident_WeekDay"] =  traffic_data["DATE_"].dt.dayofweek.astype("int")
    
    traffic_datetime["Accident_hour"] = traffic_data["TIME"].dt.hour.astype("int")
    
    return traffic_datetime

def prepare_datetime(traffic_data):
    local_traffic_data = traffic_data.copy()
    local_traffic_data["DATE_"] = pd.to_datetime(traffic_data["DATE_"], errors='coerce',
                                                 utc = True)
    local_traffic_data["TIME"] = pd.to_datetime(traffic_data["TIME"], errors='coerce',
                                                utc = True)
    local_traffic_data.drop(["DATE_","TIME"],axis = 1)
    local_traffic_data_datetime = datatime_transform(local_traffic_data)
    return [local_traffic_data,local_traffic_data_datetime]


def na_percent(data):
    # This function takes in data and returns the percentage level of NA
    na_cols = dict()
    for cname in data.columns:
        #checks na percentage level
        naPer = data[cname].isna().sum()/data[cname].count()
        if naPer >0:
            na_cols[cname] = round(naPer,3)
    return na_cols

def  check_na(data):
    #Returns a list of input coloumns that are less than 30% n/a
    cols = [cname for cname in data.columns
        if data[cname].isna().sum()/data[cname].count() <0.3 ]  
    return cols 

def drop_data_specific(traffic_data):
    """ This drops a few columns that are not needed. This is data set specific
    """
    return traffic_data.drop(["X","Y","LOCATION_EASTING_OSGR", "LOCATION_NORTHING_OSGR",
                              "ACCIDENT_INDEX","OBJECTID","DAY_OF_WEEK"], axis = 1)
    