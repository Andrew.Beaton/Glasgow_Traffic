# -*- coding: utf-8 -*-
"""
Created on Mon Feb  7 19:20:49 2022

@author: andrewb
"""

import pandas as pd 
import time 

from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import OneHotEncoder
from sklearn.compose import ColumnTransformer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score

def na_percent(data):
    # This function takes in data and returns the percentage level of NA
    na_cols = dict()
    for cname in data.columns:
        #checks na percentage level
        naPer = data[cname].isna().sum()/data[cname].count()
        if naPer >0:
            na_cols[cname] = round(naPer,3)
    return na_cols

def  check_na(data):
    #Returns a list of input coloumns that are less than 30% n/a
    cols = [cname for cname in data.columns
        if data[cname].isna().sum()/data[cname].count() <0.3 ]  
    return cols 

def extract_numeric(data):
    #returns all columns that are within the list of data types
    numerics = ["int16","int32","int64","float16","float32","float64"]
    numerical_cols = [colname for colname in data.columns if 
                      data[colname].dtype in numerics]
    
    return numerical_cols

def extract_cat_small(data,cutp):
     #Returns a list of catagorical columns which have fewer 
     #than "cutp" entry types
    cat_cols = [cname for cname in data.columns if
                    data[cname].nunique() < cutp and 
                    data[cname].dtype == "object" ]
    return cat_cols
def drop_x_y(data):
    #Drops two columns that are unused
    return data.drop(["X","Y"],axis = 1 )

def Check_Police_force(data):
    # Filter Statement for police force 
    force_filter = data["POLICE_FORCE"] !="Police Scotland"
    if  (data["POLICE_FORCE"].where(force_filter).count()) != 0 :
        print("WARNING VARIATION MISSED WITHIN POLICE FORCE")
        
    
def modelpipeline(model):
    var_pipe = Pipeline(
        steps = [("preprocessor",preprocessor),
                 ("TestedModel",model)
                 ])
    return var_pipe

#Path to traffice data source 
datapath = "Z:\\Andrew\\python\\Glasgow_Traffic\\Road_Safety_Accidents.csv"

#Loads data into pandas
raw_traffic = pd.read_csv(datapath)

#Drop x_y columns 
traffic_data = drop_x_y(raw_traffic)

#Checks if there is a police force other than police scotland
Check_Police_force(traffic_data)
na_percent(traffic_data)


#Extracts the data to be predicted 
labels = traffic_data.ACCIDENT_SEVERITY

#Removes target data from the features 
traffic_data.drop(["ACCIDENT_SEVERITY"], axis = 1,inplace=True)
features = traffic_data


#test/train splitting 
train_features, test_features, train_labels, test_labels = train_test_split(
    features,labels, train_size= 0.8, test_size=0.2, random_state = 0
    )

#Breaking up the columns into numeric and categorical 
numerical_cols = extract_numeric(train_features)
cat_cols = extract_cat_small(train_features, 10)



#Checks if the  columns can be used imputer
numerical_cols_final = check_na(train_features[numerical_cols])
cat_cols_final = check_na(train_features[cat_cols])

#Pipeline for categorical Data 
 
cat_transformer = Pipeline(steps = [
    ("imputer",SimpleImputer(strategy = "most_frequent")),
    ("onehot" , OneHotEncoder(handle_unknown = "ignore"))
    ])

#Pipeline for Numerical Data  
num_transformer =  Pipeline(steps = [
     ("imputer",SimpleImputer(strategy = "most_frequent"))
     ])

# Combine Pre processing pipelines 
preprocessor = ColumnTransformer(
    transformers = [
        ("num", num_transformer,numerical_cols_final),
        ("cat",cat_transformer,cat_cols_final)
        ])


logistic_regression =  LogisticRegression (random_state = 0)
#~~~~~~~~~~
#logistic Regression
#~~~~~~~~~~
t0=time.time()
regpipe = modelpipeline(logistic_regression)
regpipe.fit(train_features,train_labels)
predictions = regpipe.predict(test_features)

print("Accuracy from logistic Regression :")
print(accuracy_score(test_labels, predictions))
print('Time taken :' , time.time()-t0)

