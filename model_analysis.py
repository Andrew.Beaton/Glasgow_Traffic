# -*- coding: utf-8 -*-
"""
Created on Sun Apr 10 22:38:55 2022

@author: andrewb
"""
from sklearn.metrics import accuracy_score, f1_score, recall_score,precision_score
from sklearn.metrics import confusion_matrix,ConfusionMatrixDisplay

def general_overview_classification(model : object,
                     test_labels,
                     predictions
                    ):
    
    print("\n")
    print("Begining Overview of {}".format(str(model)))
    
    print("\n")
    print("Acuracy score from {}".format(str(model)))
    print(accuracy_score(test_labels, predictions))
    
    print("\n")
    print("Recall score from {} using {} average".format(str(model),"weighted"))
    print(recall_score(test_labels, predictions,average="weighted"))
    
    print("\n")
    print("precission score from {} using {}".format(str(model),"weighted"))
    print(precision_score(test_labels, predictions, average ="weighted"))
    
    print("\n")
    print("F1 score from {} using {} average".format(str(model),"weighted"))
    print(f1_score(test_labels, predictions,average="weighted"))
    print("\n")
    
def confusion_matrix_plot(model:object,
                              test_labels,
                              predictions):
    """
    

    Parameters
    ----------
    model : object
        The ML model being examined.
    test_labels : TYPE
        These are the lables split out as the test data set lables.
    predictions : TYPE
        predictions of the above labels using ML model in "model".

    Returns
    -------
    None.

    """
    # Creates the confusion matrix
    conf_m = confusion_matrix(test_labels, predictions,
                                  labels = model.classes_)
        
    #prepare confusion matrix
    display = ConfusionMatrixDisplay(confusion_matrix = conf_m,
                                         display_labels= model.classes_ )
    print("Plotting matrix for {}".format(str(model)))    
    display.plot()
        
