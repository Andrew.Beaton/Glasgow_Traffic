# -*- coding: utf-8 -*-
"""
Created on Sun Feb 13 12:42:06 2022

@author: andrewb
"""
import functions as func
import data_clean as dc
import pandas as pd 

from sklearn.model_selection import train_test_split


def extract_numeric(data):
    #returns all columns that are within the list of data types
    numerics = ["int16","int32","int64","float16","float32","float64"]
    #for colname in data.columns :
       #print(str(colname) +" ==== "+ str(data[colname].dtype))
    numerical_cols = [colname for colname in data.columns if 
                      data[colname].dtype in numerics]
    
    return numerical_cols

def extract_cat_small(data,cutp):
     #Returns a list of catagorical columns which have fewer 
     #than "cutp" entry types
    cat_cols = [cname for cname in data.columns if
                    data[cname].nunique() < cutp and 
                    data[cname].dtype == "object" ]
    return cat_cols   
    
def cleaning_initial(raw_traffic):
    """ Data cleaning -  Removes no variance cols prepares date time information
    check na percentage for over 30% imputer limit"""
    

    # Drops columns with only one value 
    traffic_data = dc.var_check(raw_traffic)
    

    #process data and time 
    traffic_post_datetime = dc.prepare_datetime(traffic_data)

    #Un comment if you need date and time info other than extracted numbers 
    #traffic_data =traffic_post_datetime[0]
    
    #time and date information as extracted ints
    traffic_datetime = traffic_post_datetime[1]
    
    


    traffic_data = pd.concat([traffic_data,traffic_datetime],axis = 1)
    
   
    traffic_data.drop(["DATE_","TIME"], axis =1 , inplace = True)
    
    traffic_data = dc.drop_data_specific(traffic_data)

    # Can be used to check percentage of results that are na 
    #dc.na_percent(traffic_data)
    
    return traffic_data 
    
def ml_prep(traffic_data):
    """Prepares data for ML - lablel extraction split,test,train coloumn
    type breakdown"""
    
    
    #Extracts the data to be predicted 
    labels = traffic_data.ACCIDENT_SEVERITY

    #Removes target data from the features 
    traffic_data.drop(["ACCIDENT_SEVERITY"], axis = 1,inplace=True)
    features = traffic_data


    #test/train splitting 
    train_features, test_features, train_labels, test_labels = train_test_split(
        features,labels, train_size= 0.8, test_size=0.2, random_state = 0,
        shuffle =  True
        )


    #Breaking up the columns into numeric and categorical 
    numerical_cols = extract_numeric(train_features)
    cat_cols = extract_cat_small(train_features, 10)



    #Checks if the  columns can be used imputer
    numerical_cols_final = dc.check_na(train_features[numerical_cols])
    Speed_col  = ["SPEED_LIMIT"]
    numerical_cols_final.remove("SPEED_LIMIT")
    cat_cols_final = dc.check_na(train_features[cat_cols])
    
    # Dictionary return of column names
    final_cols = {"numerical_cols_final":numerical_cols_final,
                  "Speed_col":Speed_col,
                  "cat_cols_final":cat_cols_final}
    
    # Dictionary of data to be used in models
    split_data = {"train_features":train_features,
                  "test_features":test_features,
                  "train_labels":train_labels,
                  "test_labels":test_labels
        }
    
    #Combines all results to be returned
    all_returns = {"final_cols":final_cols,
                   "split_data":split_data}
    
    return all_returns
