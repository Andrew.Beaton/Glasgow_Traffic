# -*- coding: utf-8 -*-
"""
Created on Sun Feb 13 17:27:13 2022

@author: andrewb
"""

import pandas as pd
import seaborn as sn
import matplotlib.pyplot as plt
import pre_process as pre_pro
import data_clean as dc
import matplotlib.pyplot as plt
 
def main():
    datapath = "Z:\\Andrew\\python\\Glasgow_Traffic\\Road_Safety_Accidents.csv"
    
    """  Created a heat map of the correlation between variables."""
    #Loads data into pandas
    raw_traffic = pd.read_csv(datapath)

    #traffic_data = pre_pro.cleaning_initial(raw_traffic)
    
    traffic_data = dc.drop_data_specific(raw_traffic)

    corrMatrix = traffic_data.corr()
    sn.heatmap(corrMatrix)
    plt.show()
    
    """Create bar plots """
    
    print(traffic_data["ACCIDENT_SEVERITY"].value_counts())
    
    traffic_data["ACCIDENT_SEVERITY"].value_counts().plot(
        kind ="bar",
        title="Accident Severity")
    plt.show()

    
    traffic_data["SPEED_LIMIT"].value_counts().plot(
        kind ="bar",
        title = "Speed Limit")
    plt.show()


    traffic_data["NUMBER_OF_VEHICLES"].value_counts().plot(
        kind ="bar",
        title = "Number of vehicles")
    plt.show()
    
    traffic_data["NUMBER_OF_CASUALTIES"].value_counts().plot( 
        kind = "bar",
        title = "Number of casualties")
    

if __name__ == "__main__":
    main()
    
    
    